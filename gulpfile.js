const gulp = require('gulp')
const groupmedia = require('gulp-group-css-media-queries')
const mincss = require('gulp-clean-css')

gulp.task("default", () => {
    return gulp.src('./style.css')
        .pipe(groupmedia())
        .pipe(
            mincss({
                compatibility: "ie8", level: {
                    1: {
                        specialComments: 0,
                        removeEmpty: true,
                        removeWhitespace: true
                    },
                    2: {
                        mergeMedia: true,
                        removeEmpty: true,
                        removeDuplicateFontRules: true,
                        removeDuplicateMediaBlocks: true,
                        removeDuplicateRules: true,
                        removeUnusedAtRules: false
                    }
                }
            })
        )
        .pipe(gulp.dest('dist/'))
});

gulp.series("default");